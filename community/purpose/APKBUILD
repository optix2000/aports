# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=purpose
pkgver=5.91.0
pkgrel=0
pkgdesc="Framework for providing abstractions to get the developer's purposes fulfilled"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
# ppc64le blocked by kaccounts-integration
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kaccounts-integration-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
checkdepends="xvfb-run"
options="!check" # fails on builders
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/purpose-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON
	cmake --build build
}

check() {
	cd build
	# This is 1 of 2 tests
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E 'menutest'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
d2bf6f99b7836ae0bfda6bfb7ee231b35952514d774b13cbbab6e6aa99b6a6e0c9148ed11ecf1c6baccd9b5e6a1566390852b069ad59f1402ac03ed2ca7ef200  purpose-5.91.0.tar.xz
"
